BitmapText
==========

|since_itg|

.. contents:: :local:

Description
-----------

A actor that renders text. Text can be aligned using :lua:meth:`Actor.horizalign` (:lua:meth:`Actor.halign` does not work).

XML attributes
--------------

**Font**

Name of the font to use (Eg: ``_eurostile outline``).

**Text**

Text that should be displayed. This can be changed later with :lua:meth:`BitmapText.settext`.

API reference
-------------

.. lua:autoclass:: BitmapText
