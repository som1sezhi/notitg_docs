FadingBanner
============

|since_itg|

.. contents:: :local:

Description
-----------

An actor that fades between song banners

API reference
-------------

.. lua:autoclass:: FadingBanner
