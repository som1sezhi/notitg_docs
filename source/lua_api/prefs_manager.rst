PrefsManager
============

|since_itg|

.. contents:: :local:

Description
-----------

The preference manager, accessible from the global :lua:attr:`PREFSMAN <global.global.PREFSMAN>`

API reference
-------------

.. lua:autoclass:: PrefsManager
