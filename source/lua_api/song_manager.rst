SongManager
===========

|since_itg|

.. contents:: :local:

Description
-----------

The song manager, accessible from the global :lua:attr:`SONGMAN <global.global.SONGMAN>`

API reference
-------------

.. lua:autoclass:: SongManager
